resource "aws_acm_certificate" "cert" {
    domain_name = "${var.site_domain}"
    validation_method = "DNS"
    provider = "aws.virginia"

}

resource "aws_acm_certificate_validation" "cert" {
    certificate_arn = "${aws_acm_certificate.cert.arn}"
    validation_record_fqdns = ["${cloudflare_record.acm_verification.hostname}"]
    provider = "aws.virginia"
}