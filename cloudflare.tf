resource "cloudflare_record" "web_cname_sub_noproxy" {
    domain = "${var.cloudflare_zone}"
    name = "${var.site_subdomain}"
    value = "${aws_cloudfront_distribution.s3_distribution.domain_name}"
    count = "${var.redirect_to_subdomain ? 1 : 0}"
    type = "CNAME"
    ttl = "1"
    proxied = "false"
}
resource "cloudflare_record" "web_cname_sub_proxy" {
    domain = "${var.cloudflare_zone}"
    name = "${var.site_subdomain}"
    value = "${aws_cloudfront_distribution.s3_distribution.domain_name}"
    count = "${var.redirect_to_apex ? 1 : 0}"
    type = "CNAME"
    ttl = "1"
    proxied = "true"
}

resource "cloudflare_record" "web_cname_apex_proxy" {
    domain = "${var.cloudflare_zone}"
    name = "${var.site_apex}"
    value = "${aws_cloudfront_distribution.s3_distribution.domain_name}"
    count = "${var.redirect_to_subdomain ? 1 : 0}"
    type = "CNAME"
    ttl = "1"
    proxied = "true"
}

resource "cloudflare_record" "acm_verification" {
    domain = "${var.cloudflare_zone}"
    name = "${aws_acm_certificate.cert.domain_validation_options.0.resource_record_name}"
    value = "${aws_acm_certificate.cert.domain_validation_options.0.resource_record_value}"
    type = "${aws_acm_certificate.cert.domain_validation_options.0.resource_record_type}"
    ttl = "1"
    proxied = "false"
}
resource "cloudflare_record" "web_cname_apex_noproxy" {
    domain = "${var.cloudflare_zone}"
    name = "${var.site_apex}"
    value = "${aws_cloudfront_distribution.s3_distribution.domain_name}"
    count = "${var.redirect_to_apex ? 1 : 0}"
    type = "CNAME"
    ttl = "1"
    proxied = "false"
}

resource "cloudflare_page_rule" "web_subdomain_redir" {
    zone = "${var.cloudflare_zone}"
    target = "http://${var.site_subdomain}.${var.site_apex}/*"
    priority = 1
    count = "${var.redirect_to_apex ? 1 : 0}"
    actions {
        forwarding_url {
            url = "https://${var.site_apex}"
            status_code = "302"
        }
    }
}

resource "cloudflare_page_rule" "web_apex_redir" {
    zone = "${var.cloudflare_zone}"
    target = "http://${var.site_apex}/*"
    priority = 1
    count = "${var.redirect_to_subdomain ? 1 : 0}"
    actions {
        forwarding_url {
            url = "https://${var.site_subdomain}.${var.site_apex}"
            status_code = "302"
        }
    }
}