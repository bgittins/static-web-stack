provider "aws" {
  region = "${var.aws_region}"
}

provider "aws" {
  region = "us-east-1"
  alias = "virginia"
}

provider "cloudflare" {
  email = "${var.cloudflare_email}"
  token = "${var.cloudflare_token}"
}

terraform {
  backend "s3" {
    bucket = "terraform-state-bg"
    region = "ap-southeast-2"
    key = "bgittinsme/terraform.tfstate"
  }
}

output "website_cname" {
    value = "${aws_cloudfront_distribution.s3_distribution.domain_name}"
}