resource "aws_s3_bucket" "site" {
    bucket = "${var.site_domain}"

    website {
        index_document = "${var.bucket_index_document}"
        error_document = "${var.bucket_error_document}"
    }

    logging {
        target_bucket = "${aws_s3_bucket.site_log_bucket.id}"
    }

    versioning {
        enabled = true
    }
    
    acl = "public-read"
    policy = <<POLICY
{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Sid":"AddPerm",
      "Effect":"Allow",
      "Principal": "*",
      "Action":["s3:GetObject"],
      "Resource":["arn:aws:s3:::${var.site_domain}/*"]
    }
  ]
}
POLICY

server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
 }

}

resource "aws_s3_bucket" "site_log_bucket" {
    bucket = "${var.site_domain}-logs"
    acl = "log-delivery-write"

    server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
 }
}