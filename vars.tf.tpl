variable "site_domain" {
    default = "#######" # Enter whatever primary domain you want here e.g. if you want www.test.com or test.com
}

variable "site_apex" {
    default = "#######" # Enter apex domain here e.g. test.com
}

variable "site_subdomain"{
    default = "#######" # Enter subdomain here e.g. www
}

variable "redirect_to_apex"{
    default = false # This permits you to redirect to the Apex domain e.g. test.com it uses a Cloudflare Page Rule to do so
}

variable "redirect_to_subdomain" {
    default = true # This permits you to redirect to a subdomain e.g. www.test.com it uses a Cloudflare page rule to do so
}

variable "bucket_index_document"{
    default = "index.html"
}

variable "bucket_error_document"{
    default = "404.html"
}

variable "aws_region" {
    default = "########"
}

variable "cloudflare_email" {
    default = "########"
}

variable "cloudflare_token" {
    default = "########"
}

variable "cloudflare_zone" {
    default = "########"
}
